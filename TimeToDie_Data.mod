<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">	
  <UiMod name="TimeToDie_Data" version="1.0" date="12/11/2008" >		
    <Author name="Aiiane" email="aiiane@aiiane.net" />		
    <Description text="Saved data for TimeToDie." />       
    <VersionSettings gameVersion="1.9.9" />                 
    <Dependencies>            
      <Dependency name="TimeToDie" />        
    </Dependencies>                 
    <SavedVariables>            
      <SavedVariable name="TimeToDie.DeathLog" />        
    </SavedVariables>		 	
  </UiMod>
</ModuleFile>