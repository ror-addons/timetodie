2008-11-12  aiiane  <aiiane@aiiane.net>

	* .pkgmeta, TimeToDie.lua, TimeToDie.mod, TimeToDie_Data.mod:
	Initial commit. Collects and reports on information about your
	characters' deaths.
	[8fec5ac0c329] [1.0]

