<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">	   
  <UiMod name="TimeToDie" version="1.0" date="12/11/2008" >		     
    <Author name="Aiiane" email="aiiane@aiiane.net" />		     
    <Description text="Data-gathering addon that tracks info about deaths.&lt;br&gt;To view data enter:
      &lt;br&gt;/ttd - average
      &lt;br&gt;/ttdlast - last die
      &lt;br&gt;/ttdreset - reset all data" />               
      <VersionSettings gameVersion="1.9.9" />        		     
      <Files>                   
        <File name="TimeToDie.lua" />		     
      </Files>		 		     
      <OnInitialize>                   
        <CallFunction name="TimeToDie.Initialize" />             
      </OnInitialize>		     
      <OnUpdate>                   
        <CallFunction name="TimeToDie.OnUpdate" />             
      </OnUpdate>		     
      <OnShutdown/>		 	   
  </UiMod>
</ModuleFile>