TimeToDie = {}

local function print(stuff)
    EA_ChatWindow.Print(towstring(stuff))
end

-- How long we consider us to still be 'in combat' with no combat events directed towards ourself
local combatEventWindow = 10

-- How many combat events do we save per death?
local combatEventLimit = 10

-- How many deaths to keep info for
local deathLogSize = 100

-- table to keep track of death information
TimeToDie.DeathLog = {}

-- table to keep track of current status info
TimeToDie.LifeLog = {}

-- time counter
local timeCount = 0

local atFullHealth = false

function TimeToDie.NewLife()
    TimeToDie.LifeLog = {
        startTime = timeCount,
        endTime = timeCount,
        duration = 0,
        combatEvents = {},
        level = GameData.Player.level,
        careerLine = GameData.Player.career.line,
        careerName = GameData.Player.career.name,
    }
    TimeToDie.PlayerHealthUpdate()
end

function TimeToDie.PushDeath()
    -- rebase timestamps so they show offset from death time
    for k,v in ipairs(TimeToDie.LifeLog.combatEvents) do
        TimeToDie.LifeLog.combatEvents[k].time = TimeToDie.LifeLog.combatEvents[k].time - timeCount
    end
    
    table.insert(TimeToDie.DeathLog, TimeToDie.LifeLog)
    
    if #TimeToDie.DeathLog > deathLogSize then
        table.remove(TimeToDie.DeathLog, 1)
    end
    
    TimeToDie.NewLife()
end

function TimeToDie.OnUpdate(timePassed)
    timeCount = timeCount + timePassed
end

function TimeToDie.PlayerDeath()
    -- Ignore the false positive from zoning
    if SystemData.LoadingData.isLoading == true then return end
    
    if GameData.Player.killerName ~= L"" then
        TimeToDie.LifeLog.killer = GameData.Player.killerName
    end
    TimeToDie.LifeLog.endTime = timeCount
    TimeToDie.LifeLog.duration = timeCount - TimeToDie.LifeLog.startTime
    if TimeToDie.LifeLog.fightStartTime then
        TimeToDie.LifeLog.fightDuration = timeCount - TimeToDie.LifeLog.fightStartTime
    end
    if TimeToDie.LifeLog.fullHealthTime then
        TimeToDie.LifeLog.healthDuration = timeCount - TimeToDie.LifeLog.fullHealthTime
    end
    TimeToDie.PushDeath()
end

function TimeToDie.PlayerHealthUpdate()
    if GameData.Player.hitPoints.maximum <= 0 then return end
    if GameData.Player.hitPoints.current <= 0 then return end
    if GameData.Player.hitPoints.current == GameData.Player.hitPoints.maximum then
        atFullHealth = true
    else
        if atFullHealth == true then
            TimeToDie.LifeLog.fullHealthTime = timeCount
            atFullHealth = false
        end
    end
end

function TimeToDie.CombatEvent(targetId, hitAmount, hitType)
    if targetId ~= GameData.Player.worldObjNum then return end
    
    if (not TimeToDie.LifeLog.lastCombatEvent) or
      (TimeToDie.LifeLog.lastCombatEvent + combatEventWindow < timeCount) then
        
        TimeToDie.LifeLog.fightStartTime = timeCount
    end
    
    table.insert(TimeToDie.LifeLog.combatEvents, {type=hitType, amount=hitAmount, time=timeCount})
    if #TimeToDie.LifeLog.combatEvents > combatEventLimit then
        table.remove(TimeToDie.LifeLog.combatEvents, 1)
    end
    TimeToDie.LifeLog.lastCombatEvent = timeCount    
end

function TimeToDie.LastDeathReport()
    if #TimeToDie.DeathLog <= 0 then
        print("TIMETODIE: No deaths logged.")
        return
    end
    
    local last = TimeToDie.DeathLog[#TimeToDie.DeathLog]
    
    print("====================================")
    print("TIMETODIE REPORT [LAST DEATH]")
    print(L"Data for level "..towstring(last.level)..L" "..last.careerName)
    print("------------------------------------")
    
    if last.killer then
        print(L"Killer's name: "..last.killer)
    end
    
    print("Total lifetime: "..string.format("%0.1f", last.duration).." seconds")
    
    if last.fightDuration then
        print("Duration of final combat: "..string.format("%0.1f", last.fightDuration).." seconds")
    end
    
    if last.healthDuration then
        print("Time for 100% --> 0% hp: "..string.format("%0.1f", last.healthDuration).." seconds")
    end
    
    print("====================================")
end

function TimeToDie.AverageReport()
    if #TimeToDie.DeathLog <= 0 then
        print("TIMETODIE: No deaths logged.")
        return
    end
    
    local avgTotalLife, sumTotalLife, minTotalLife, maxTotalLife, countTotalLife = 0, 0, 10000000000, 0, 0
    local avgFightDuration, sumFightDuration, minFightDuration, maxFightDuration, countFightDuration = 0, 0, 10000000000, 0, 0
    local avgHealthDuration, sumHealthDuration, minHealthDuration, maxHealthDuration, countHealthDuration = 0, 0, 10000000000, 0, 0
    
    local killers = {}
    
    for k,v in ipairs(TimeToDie.DeathLog) do
        -- Toss out anything that's not of our current career and level
        if v.careerLine == GameData.Player.career.line and v.level == GameData.Player.level then
        
            if v.killer then
                killers[v.killer] = killers[v.killer] and killers[v.killer]+1 or 1
            end
        
            if v.duration then
                countTotalLife = countTotalLife + 1
                sumTotalLife = sumTotalLife + v.duration
                minTotalLife = math.min(v.duration, minTotalLife)
                maxTotalLife = math.max(v.duration, maxTotalLife)
            end
            
            if v.fightDuration then
                countFightDuration = countFightDuration + 1
                sumFightDuration = sumFightDuration + v.fightDuration
                minFightDuration = math.min(v.fightDuration, minFightDuration)
                maxFightDuration = math.max(v.fightDuration, maxFightDuration)
            end
            
            if v.healthDuration then
                countHealthDuration = countHealthDuration + 1
                sumHealthDuration = sumHealthDuration + v.healthDuration
                minHealthDuration = math.min(v.healthDuration, minHealthDuration)
                maxHealthDuration = math.max(v.healthDuration, maxHealthDuration)
            end
            
        end
    end
    
    if countTotalLife > 0 then
        avgTotalLife = sumTotalLife / countTotalLife
    end
    
    if countFightDuration > 0 then
        avgFightDuration = sumFightDuration / countFightDuration
    end
    
    if countHealthDuration > 0 then
        avgHealthDuration = sumHealthDuration / countHealthDuration
    end
    
    local highKills = 0
    local highKillers = {}
    for k,v in pairs(killers) do
        if v == highKills then
            table.insert(highKillers, k)
        elseif v > highKills then
            highKillers = {k}
        end
    end
    
    -- Done calculating, now display it all
    
    print("====================================")
    print("TIMETODIE REPORT [AVERAGES]")
    print(L"Data for level "..towstring(GameData.Player.level)..L" "..GameData.Player.career.name)
    print("------------------------------------")
    
    print("Average total lifetime: "..string.format("%0.1f", avgTotalLife).." seconds ("..countTotalLife.." datapts)")
    
    if countFightDuration > 0 then
        print("Average duration of final combat: "..string.format("%0.1f", avgFightDuration).." seconds ("..countFightDuration.." datapts)")
    end
    
    if countHealthDuration > 0 then
        print("Average time for 100% --> 0% hp: "..string.format("%0.1f", avgHealthDuration).." seconds ("..countHealthDuration.." datapts)")
    end
    
    print("====================================")
end

function TimeToDie.Reset()
    TimeToDie.DeathLog = {}
    TimeToDie.NewLife()
    print("TimeToDie data has been reset.")
end

local firstLoad = true
function TimeToDie.LoadingEnd()
    if firstLoad then
        firstLoad = false
        
        if LibSlash then
            LibSlash.RegisterWSlashCmd("ttd", TimeToDie.AverageReport)
            LibSlash.RegisterWSlashCmd("timetodie", TimeToDie.AverageReport)
            LibSlash.RegisterWSlashCmd("ttdlast", TimeToDie.LastDeathReport)
            LibSlash.RegisterWSlashCmd("ttdreset", TimeToDie.Reset)
        end
    end
    
    TimeToDie.NewLife()
end

function TimeToDie.Initialize()
    -- some pertinent events
    RegisterEventHandler(SystemData.Events.PLAYER_DEATH, "TimeToDie.PlayerDeath")
    RegisterEventHandler(SystemData.Events.PLAYER_CUR_HIT_POINTS_UPDATED, "TimeToDie.PlayerHealthUpdate")
    RegisterEventHandler(SystemData.Events.PLAYER_MAX_HIT_POINTS_UPDATED, "TimeToDie.PlayerHealthUpdate")
    RegisterEventHandler(SystemData.Events.WORLD_OBJ_COMBAT_EVENT, "TimeToDie.CombatEvent")

    RegisterEventHandler(SystemData.Events.LOADING_END, "TimeToDie.LoadingEnd")
    RegisterEventHandler(SystemData.Events.RELOAD_INTERFACE, "TimeToDie.LoadingEnd")
    
    TimeToDie.NewLife()
end